<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//xNyLiSsA512Bo2ZXte9C8TupS5EUlCr6YT05NoFo
//dd(str_random(40));
//dd(Hash::make('123456'));
Route::get('/', function () {
    return view('welcome');
});

Route::post('oauth/access_token','Api\v1\AuthController@authorizeUser');

Route::group(['prefix'=>'v1','middleware' => ['cors','oauth']], function(){

	Route::get('menus/todos','Api\v1\MenuController@listAll');
	Route::resource('users','Api\v1\UsersController',['only' => ['store','index','update','show','destroy']]);
	Route::resource('products','Api\v1\ProductController',['only' => ['store','index','update','show','destroy']]);
	Route::resource('menus','Api\v1\MenuController',['only' => ['store','index','update','show','destroy']]);
	Route::resource('tables','Api\v1\TableController',['only' => ['store','index','update','show','destroy']]);
	Route::resource('upload','Api\v1\UploadController',['only' => ['store','destroy']]);
	Route::resource('qr-code','Api\v1\QrCodeController',['only' => ['index']]);
	Route::resource('roles','Api\v1\RoleController',['only' => ['index']]);
	Route::resource('establishment','Api\v1\EstablishmentController',['only' => ['update','edit']]);
});
