<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FastOrder\Product\ProductRepository;

class ProductController extends Controller
{
    private $repository;

    public function __construct(ProductRepository $repository){
        $this->repository = $repository;
    }

    public function index()
    {
        $total = $this->repository->count();
        $data = $this->repository->allPaginated(25)->all();
        return response()->json(compact('total','data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\ProductRequest $request)
    {
         try{
            $data = $request->all();
            $this->repository->create($data);
            return response()->json(['message'=> 'Produto cadastrado com sucesso!'],201);
         }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],400);
         }
    }

    public function show($id)
    {
        try{
            $data = $this->repository->find($id);
            return response()->json($data);
        }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],400);
        }
    }
    public function update(Request $request, $id)
    {
         try{
            $data = $this->repository->find($id);
            $data->update($request->all());
            return response()->json(['message'=> 'Produto alterado com sucesso!'],200);
         }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],400);
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
          try{
            $this->repository->delete($id);
            return response()->json(['message'=> 'Produto excluido com sucesso!'],200);
         }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],400);
         }
    }
}
