<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FastOrder\User\UserRepository;

class UsersController extends Controller
{
    private $repository;

    public function __construct(UserRepository $repository){
        $this->repository = $repository;
    }

    public function index()
    {
      try{
        $total = $this->repository->count();
        $data = $this->repository->allPaginated(25)->all();
        return response()->json(compact('total','data'));
      }catch(\Exception $e){
         \Log::critical($e);
         return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],500);
      }
    }

    public function store(Requests\UserRequest $request)
    {
         try{
            $this->repository->create($request->all());
            return response()->json(['message'=> 'Usuário cadastrado com sucesso!'],201);
         }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],500);
         }
    }

    public function show($id)
    {
        try{
            $data = $this->repository->find($id);
            return response()->json($data);
        }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],500);
        }
    }

    public function update(Requests\UserEditRequest $request, $id)
    {
         try{
            $data = $this->repository->find($id);
            $data->update($request->all());
            return response()->json(['message'=> 'Usuário alterado com sucesso!'],200);
         }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],500);
         }
    }

    public function destroy($id)
    {
          try{
            $this->repository->delete($id);
            return response()->json(['message'=> 'Usuário excluido com sucesso!'],200);
         }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],500);
         }
    }
}
