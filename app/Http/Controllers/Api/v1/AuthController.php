<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use FastOrder\User\User;
use FastOrder\Auth\OAuthClient;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class AuthController extends Controller
{
    public function authorizeUser(Request $request) {
      $login = $request->get('username');
      $establishmentId = User::where('login','=',$login)->first(['establishment_id'])->establishment_id;
      $oauthData = OAuthClient::where('id',$establishmentId)->first(['id','secret']);
      $request->merge(['client_id'=>$oauthData->id, 'client_secret'=>$oauthData->secret]);
      return response()->json(Authorizer::issueAccessToken());
    }
  }
