<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FastOrder\Roles\RolesList;
class RoleController extends Controller
{
    private $roles;

    public function __construct(RolesList $rolesList){
        $this->roles = $rolesList;
    }

    public function index()
    {
        return response()->json($this->roles->getAdministrativeRoles());
    }
}
