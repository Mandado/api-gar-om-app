<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use QrCode;

class QrCodeController extends Controller
{
	public function index(Request $request)
	{
		$name = $request->get('name');
		if($name){
			$qrCode = QrCode::format('png')->size(1024)->generate($name);
			return response($qrCode)
			->header('Content-Type', 'application/octet-stream')
			->header('Content-Disposition', 'attachment; filename='.$name.'.png');
		}else{
			return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],400);	
		}
	}
}
