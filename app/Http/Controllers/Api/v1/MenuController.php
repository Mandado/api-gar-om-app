<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FastOrder\Menu\MenuRepository;

class MenuController extends Controller
{
    private $repository;

    public function __construct(MenuRepository $repository){
        $this->repository = $repository;
    }

    public function index()
    {
        $total = $this->repository->count();
        $data = $this->repository->allPaginated(25)->all();
        return response()->json(compact('total','data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\MenuRequest $request)
    {
         try{
            $this->repository->create($request->all());
            return response()->json(['message'=> 'Cardápio cadastrado com sucesso!'],201);
         }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],400);
         }
    }

    public function show($id)
    {
        try{
            $data = $this->repository->find($id);
            return response()->json($data);
        }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],400);
        }
    }
    public function update(Requests\MenuRequest $request, $id)
    {
         try{
            $data = $this->repository->find($id);
            $data->update($request->all());
            return response()->json(['message'=> 'Cardápio alterado com sucesso!'],200);
         }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],400);
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
          try{
            $this->repository->delete($id);
            return response()->json(['message'=> 'Cardápio excluido com sucesso!'],200);
         }catch(\Exception $e){
            \Log::critical($e);
            return response()->json(['error'=>true,'message'=>'Erro por favor, tente mais tarde.'],400);
         }
    }

    public function listAll(){
        return response()->json($this->repository->all(['id','name']));
    }
}
