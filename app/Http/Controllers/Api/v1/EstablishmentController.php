<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FastOrder\Establishment\EstablishmentRepository;
use FastOrder\Establishment\Establishment;
class EstablishmentController extends Controller
{
    private $repository;

    public function __construct(EstablishmentRepository $repository){
        $this->repository = $repository;
    }

    public function edit($id)
    {
      //\FastOrder\Establishment\Establishment::create(['name'=>'fastorder','telephone'=>'3455435435','cnpj'=>'43545','street'=>'rua','neighborhood'=>'bairro0','city'=>'jf','state'=>'4545','number'=>'34343']);
      $data = $this->repository->find($id);
      return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
