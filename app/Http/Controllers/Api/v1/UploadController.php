<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FastOrder\Upload\UploadService;
use App;

class UploadController extends Controller
{
    private $uploadService;

    public function __construct(){
        $this->uploadService = App::make(UploadService::class);
    }

    public function store(Request $request){
        $response = $this->uploadService->upload($request->file('photo'));

        return response()->json($response['secure_url']);
    }

    public function destroy($publicId){
    	try{
	    	$response = $this->uploadService->delete($publicId);
	    	return response()->json(['destroyed'=>$response]);
    	}catch(\Exeception $e){
	    	return response()->json(['errors'=>true, 'message'=>$e->getMessage()]);
    		\Log::critical($e);
    	}
    }
}
