<!DOCTYPE html>
<html ng-app="fastorder">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Boxed Layout</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{url('assets/admin/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/admin/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/admin/css/dashboard.css')}}">
    <link rel="stylesheet" href="{{url('assets/admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/admin/bower_components/humane-js/themes/flatty.css')}}">
    <link rel="stylesheet" href="{{url('assets/admin/css/skins/skin-blue.min.css')}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5h elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div app-view-segment="0"></div>

    <!-- jQuery 2.1.4 -->
    <script src="{{url('assets/admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/angular/angular.min.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/angular-route-segment/build/angular-route-segment.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/angular-route/angular-route.min.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/angular-paging/dist/paging.min.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/angular-cookies/angular-cookies.min.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/angular-resource/angular-resource.min.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/humane-js/humane.min.js')}}"></script>
    <script src="{{url('assets/admin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/admin/bower_components/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{url('assets/admin/js/ltetheme.min.js')}}"></script>
    <!-- directives -->
    <script src="{{url('app/directives/masks-directive.js')}}"></script>
    <script src="{{url('app/directives/upload-directive.js')}}"></script>

    <!-- services -->
    <script src="{{url('app/services/formatService.js')}}"></script>

    <!-- home -->
    <script src="{{url('app/modules/home/home.js')}}"></script>
    <script src="{{url('app/modules/home/controllers/homeController.js')}}"></script>

    <!-- tables -->
    <script src="{{url('app/modules/products/products.js')}}"></script>
    <script src="{{url('app/modules/products/services/productsService.js')}}"></script>
    <script src="{{url('app/modules/products/controllers/productsController.js')}}"></script>

    <!-- tables -->
    <script src="{{url('app/modules/tables/tables.js')}}"></script>
    <script src="{{url('app/modules/tables/services/tablesService.js')}}"></script>
    <script src="{{url('app/modules/tables/controllers/tablesController.js')}}"></script>

    <!-- menus -->
    <script src="{{url('app/modules/menus/menus.js')}}"></script>
    <script src="{{url('app/modules/menus/services/menusService.js')}}"></script>
    <script src="{{url('app/modules/menus/controllers/menusController.js')}}"></script>

    <!-- login -->
    <script src="{{url('app/modules/login/login.js')}}"></script>
    <script src="{{url('app/modules/login/controllers/loginController.js')}}"></script>
    <!-- dashboard -->
    <script src="{{url('app/modules/dashboard/dashboard.js')}}"></script>

    <!-- establishment -->
    <script src="{{url('app/modules/establishment/establishment.js')}}"></script>
    <script src="{{url('app/modules/establishment/services/establishmentService.js')}}"></script>
    <script src="{{url('app/modules/establishment/services/usersService.js')}}"></script>
    <script src="{{url('app/modules/establishment/services/rolesService.js')}}"></script>
    <script src="{{url('app/modules/establishment/controllers/establishmentController.js')}}"></script>

    <script src="{{url('app/app.js')}}"></script>
  </body>
</html>
