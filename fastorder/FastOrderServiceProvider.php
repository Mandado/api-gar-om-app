<?php

namespace FastOrder;

use Illuminate\Support\ServiceProvider;

class FastOrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\FastOrder\Auth\Contracts\AuthServiceContract::class,\FastOrder\Auth\AuthService::class);
    }
}
