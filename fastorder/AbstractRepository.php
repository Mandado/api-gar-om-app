<?php

namespace FastOrder;

abstract class AbstractRepository
{
	protected $model;

	public function allPaginated($perPage = 5){
		return $this->model->paginate($perPage);
	}

	public function all($columns = ['*']){
		return $this->model->all($columns);
	}

	public function count(){
		return $this->model->all()->count();
	}

	public function create(array $data){
		return $this->model->create($data);
	}

	public function find($id, $columns = ['*']){
		return $this->model->findOrFail($id, $columns);
	}


	public function delete($id){
		return $this->find($id)->delete();
	}
}