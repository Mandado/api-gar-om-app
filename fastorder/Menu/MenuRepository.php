<?php

namespace FastOrder\Menu;

use FastOrder\AbstractRepository;
use App;

class MenuRepository extends AbstractRepository{
	protected $model;
	public function __construct(){
		$this->model = App::make(Menu::class);
	}
}