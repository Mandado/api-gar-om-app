<?php
namespace FastOrder\Roles;

class RolesList {
    private $roles;

    public function __construct(){
      $this->roles = collect([
          ['id'=> 'user', 'role' => 'Usuário'],
          ['id'=> 'manager', 'role' => 'Gerente'],
          ['id'=> 'administrator', 'role' => 'Administrador']
      ]);
    }

    public function getRoles(){
      return $this->roles;
    }
    public function getAdministrativeRoles(){
      return $this->roles->forget(0);
    }

    public function getRole($value){
      return $this->roles->where('id', $value);
    }
}
