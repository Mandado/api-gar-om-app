<?php
namespace FastOrder\Auth\Contracts;
/**
 * Interface AuthService
 */
interface AuthServiceContract{


    /**
     * @param array $credentials
     * @return mixed
     */
    public function authenticate($username, $password);
}