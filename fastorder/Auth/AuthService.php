<?php

namespace FastOrder\Auth;
use FastOrder\Auth\Contracts\AuthServiceContract;
use Illuminate\Support\Facades\Auth;

class AuthService implements AuthServiceContract
{
    /**
     * @param array $credentials
     * @return mixed
     */
    public function authenticate($username, $password)
    {

        $credentials = [
            'login' => $username,
            'password' => $password
        ];

        if (Auth::once($credentials)) {
            return Auth::user()->id;
        }

        return false;
    }
}
