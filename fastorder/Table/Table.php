<?php

namespace FastOrder\Table;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = ['name'];
}
