<?php
namespace FastOrder\Table;

use App;
use FastOrder\AbstractRepository;

class TableRepository extends AbstractRepository{
	protected $model;

    public function __construct(){
			
		$this->model = App::make(Table::class);
	}

}
