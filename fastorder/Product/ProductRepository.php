<?php
namespace FastOrder\Product;

use App;
use FastOrder\Upload\UploadService;
use FastOrder\AbstractRepository;

class ProductRepository extends AbstractRepository{
	protected $model;
    private $uploadService;

    public function __construct(){
        $this->uploadService = App::make(UploadService::class);
		$this->model = App::make(Product::class);
	}

	public function delete($id){
		$product = $this->find($id);
		if($product->photo && $product->photo === 'C:\fakepath\images.jpg'){
			$imageId = explode('.',array_slice(explode('/',$product->photo),-1)[0])[0];
	    	$this->uploadService->delete($imageId);
		}
		return $product->delete();
	}

}