<?php

namespace FastOrder\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','price','menu_id','ingredients','photo','personalizable','approximate_time_of_preparation'];

    public function setIngredientsAttribute($value){
    	$this->attributes['ingredients'] = json_encode($value);
    }

    public function getIngredientsAttribute(){
    	return json_decode($this->attributes['ingredients']);
    }

}
