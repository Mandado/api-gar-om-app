<?php

namespace FastOrder\Establishment;
use FastOrder\Auth\OAuthClient;
use FastOrder\User\User;
use Illuminate\Database\Eloquent\Model;

class Establishment extends Model
{
    public $fillable = ['name','cnpj','street','neighborhood','city','state','number','telephone'];

    public function users(){
      return $this->hasMany(User::class);
    }

    public function oauthUser(){
      return $this->hasOne(OAuthClient::class,'id');
    }
}
