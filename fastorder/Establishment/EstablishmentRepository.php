<?php

namespace FastOrder\Establishment;

use FastOrder\AbstractRepository;
use App;

class EstablishmentRepository extends AbstractRepository{
	protected $model;
	public function __construct(){
		$this->model = App::make(Establishment::class);
	}
}
