<?php

namespace FastOrder\User;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use FastOrder\Establishment\Establishment;
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'users';
    protected $hidden = ['password', 'remember_token'];
    protected $fillable = ['name','email','login','password','telephone','affiliate','role','establishment_id'];

    public function setPasswordAttribute($value){
      $this->attributes['password'] = \Hash::make($value);
    }

    public function establishment(){
      return $this->belongsTo(Establishment::class);
    }

}
