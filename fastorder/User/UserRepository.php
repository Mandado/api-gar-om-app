<?php

namespace FastOrder\User;

use FastOrder\AbstractRepository;
use App;

class UserRepository extends AbstractRepository{
	protected $model;
	public function __construct(){
		$this->model = App::make(User::class);
	}

	public function allPaginated($page = 10){
		return $this->model->with('establishment')->paginate($page);
	}
}
