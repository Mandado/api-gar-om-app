<?php

namespace FastOrder\Upload;
use \Cloudder;

class UploadService{

	public function upload($file){
		return Cloudder::upload($file,md5(uniqid(time())),
            ["width" => 100, "height" => 150, "crop" => "fill"]
        )->getResult();
	}

	public function delete($publicId){
		return Cloudder::delete($publicId,["width" => 100, "height" => 150, "crop" => "fill"] );
	}
}